
'use strict';


function screenScaleFactor() {
  // detect if the screen is a retina display and return the scale.
  if ('devicePixelRatio' in window) {
    if (window.devicePixelRatio > 1) {
      return window.devicePixelRatio;
    }
  }
  return 1;
}


function load_view() {
  errL('load_view...');
  // set up the canvas context and initialize size variables.
  var canvas = document.getElementById('canvas');
  var canvas_size = Vec(canvas.width, canvas.height);
  var scale_factor = screenScaleFactor();
  // to render at full resolution on a high-res display,
  // the canvas style must be set to the dimension in 'points.'
  canvas.style.width = canvas_size.x + 'px';
  canvas.style.height = canvas_size.y + 'px';
  // then the canvas size is scaled to the dimension in pixels.
  canvas.width *= scale_factor;
  canvas.height *= scale_factor;

  var ctx = canvas.getContext('2d');
  ctx.scale(scale_factor, scale_factor); // scale the rendering context for high-res displays.

  function draw_line(v0, v1) {
    // convenience function to draw a single line.
    ctx.beginPath();
    ctx.moveTo(v0.x, v0.y);
    ctx.lineTo(v1.x, v1.y);
    ctx.closePath();
    ctx.stroke();
  }

  function stroke_circle(p, radius) {
    ctx.beginPath();
    ctx.arc(p.x, p.y, radius, 0, pi2, true);
    ctx.closePath();
    ctx.stroke();
  }

  function fill_circle(p, radius) {
    ctx.beginPath();
    ctx.arc(p.x, p.y, radius, 0, pi2, true);
    ctx.closePath();
    ctx.fill();
  }

  function vel_vector_log_scaled(v, scale) {
    // return a new vector with the same direction as v,
    // but with the magnitude scaled such that for speeds >= 1, the magnitude is logarithmic.
    var s = mag(v);
    if (s < 1) return v;
    var a = angle(v);
    l = 1 + log2(s) * scalar_of_speed_log;
    return Vec_with_MA(l * scale, a);
  }

  function draw_obj(obj, world_scale, camera_pos, is_hud_mode) {
    // remember the current transformation matrix (pushes a new matrix onto the matrix stack).
    ctx.save();
    ctx.translate(obj.p.x, obj.p.y); // move to the obj center as if it is the origin.

    if (is_hud_mode) {
      // undo the world transform to get back to screen pixel space.
      //ctx.translate(camera_pos.x, camera_pos.y);
      s = 1 / world_scale;
      ctx.scale(s, s, s);

      // collision bounds
      if (obj.collided) {
        ctx.strokeStyle = '#FF0000';
      } else {
        ctx.strokeStyle = '#FFFFFF';
      }
      ctx.lineWidth = 1;
      stroke_circle(Vec(0, 0), render_dbg_rad);

      // velocity vector.
      ctx.strokeStyle = '#B00000';
      ctx.lineWidth = 3;
      draw_line(Vec(0, 0), vel_vector_log_scaled(obj.v, render_dbg_rad));

      // orientation dot.
      ctx.save();
      ctx.rotate(obj.ap);
      ctx.fillStyle = '#004000';
      fill_circle(Vec(render_dbg_rad, 0), 2);
      ctx.restore();

      // center point.
      //ctx.fillStyle = '#004000';
      //fill_circle(Vec(0, 0), 2);

      // number
      ctx.save();
      ctx.fillStyle = '#FFFFFF';
      ctx.fillText(obj.i + 1, 0, 0);
      ctx.restore();
    } else { // regular mode.
      ctx.fillStyle = '#404000';
      fill_circle(Vec(0, 0), obj.r);
    }
    ctx.restore();
  }


  function draw_info(anim_time, anim_interval) {
    // setup for info overlay drawing.
    ctx.font = '12pt Menlo';
    ctx.textAlign = 'left';
    ctx.textBaseline = 'top';
    ctx.fillStyle = '#FFFFFF';
    ctx.fillText(sprintf('T:%.2f SI:%.2f', anim_time, anim_interval), 5, 5);
  }

  function clear_canvas() {
    ctx.clearRect(0, 0, canvas_size.x, canvas_size.y);
  }

  //  ctx.save(); // save the matrix state.
  //  ctx.translate(canvas_size.x * .5, canvas_size.y * .5); // move origin to the center.
  //  var unit_scale = canvas_size.y / 2; // scale by half of canvas height to get unit scale on the short axis.
  //  var world_scale = unit_scale * camera_scale;
  //  ctx.scale(world_scale, world_scale, world_scale);
  //  ctx.translate(-camera_pos.x, -camera_pos.y);

  //  ctx.font = '12pt Menlo';
  //  ctx.textAlign = 'center';
  //  ctx.textBaseline = 'middle';
  //  ctx.restore();

  return {
    'canvas': canvas,
    'ctx': ctx,
    'clear_canvas': clear_canvas,
    'draw_line': draw_line,
    'stroke_circle': stroke_circle,
    'fill_circle': fill_circle,
  };
}
 
