
'use strict';


// opaque identifier for render_callback.
// we can use this to remove the callback and end the simulation at any time.
var interval_callback_id = -1;

var render_mode_debug = true;
var render_dbg_rad = 10;


function load() {
  // main program setup.
  errL('load()...');

  var view = load_view();

  var anim_time = 0; // absolute animation time.
  var anim_interval = .1;
  var camera_scale = 1 / 1;
  var camera_pos = Vec(0, 0);

  function main_timer_callback() {
    // the main animation callback; triggered by the window.setInterval timer.
    try {
      // the animation never skips steps.
      // instead if the browser drops a callback, the animation freezes.
      // if we wanted a realtime animation, we would measure elapsed time between frames.
      anim_time += anim_interval; // update the animation time first.
      // TODO: do something.
    }
    catch(exc) {
      errL(exc);
      stop();
      throw exc;
    }
  }


  function setup_animation() {
    // install main_timer_callback.
    if (interval_callback_id != -1) { // avoid installing multiple timers.
      errL('ERROR: already have interval_callback_id')
      return;
    }
    interval_callback_id = window.setInterval(main_timer_callback, anim_interval * msec_per_sec);
  }

  function teardown_animation() {
    // remove main_timer_callback.
    if (interval_callback_id != -1) { // there may not be any existing timer to remove.
      window.clearInterval(interval_callback_id);
      interval_callback_id = -1;
    }
  }
 
  function update_animation() {
    // if we are currently animating, remove the existing timer and reinstall with new interval.
    if (interval_callback_id != -1) {
      teardown_animation();
      setup_animation();
    }
  }

  function toggle_animation() {
    if (interval_callback_id == -1) { // start rendering.
      errL('starting.');
      setup_animation();
    } else { // stop rendering.
      errL('stopping.');
      teardown_animation();
    }
  }

  function keyPress(event) {
    // handle key events.
    var c = String.fromCharCode(event.charCode);
    var k = event.keyCode;
    if (k == 8800) { // 'opt ='; ignored.

    } else if (k == 8211) { // 'opt -'; ignored.

    } else if (c == ' ') { // toggle animation.
      toggle_animation();
    } else if (c == 'd') { // toggle debug mode.
      render_mode_debug = !render_mode_debug;
    } else if (c == '=') { // zoom in.
      camera_scale *= 1.1;
    } else if (c == '-') { // zoom out.
      camera_scale /= 1.1;
    } else if (c == '+') { // speed up animation.
      anim_interval /= 1.1;
      update_animation();
    } else if (c == '_') { // slow down animation.
      anim_interval *= 1.1;
      update_animation();
    }
    if (interval_callback_id == -1) { // not animating.
      //render();
    }
  }

  function keyDown(event) {
    // handle arrow key events and any others that do not get passed to keyPress().
    var code = event.keyCode;
    var cpx = camera_pos.x;
    var cpy = camera_pos.y;
    if (code == 37) { // left.
      camera_pos = Vec(cpx - 1 / (camera_scale * 32), cpy);
    } else if (code == 38) { // top.
      camera_pos = Vec(cpx, cpy - 1 / (camera_scale * 32));
    } else if (code == 39) { // right.
      camera_pos = Vec(cpx + 1 / (camera_scale * 32), cpy);
    } else if (code == 40) { // bottom.
      camera_pos = Vec(cpx, cpy + 1 / (camera_scale * 32));
    }
    //errL('camera: t:({0}, {1}); s:{2}'.fmt(camera_pos.x, camera_pos.y, camera_scale));
    if (interval_callback_id == -1) { // not animating.
      //render();
    }
  }

  function draw(event) {
    let v = Vec(event.offsetX, event.offsetY)
    view.ctx.fillStyle = '#FF0000';
    view.fill_circle(v, 10)
  }

  function canvasMouseDown(event) {
    draw(event)
  }

  function canvasMouseMove(event) {
    let button_mask_left = 1;
    if (event.which & button_mask_left) {
      draw(event)
    }
  }


  // install listeners.
  window.addEventListener('keypress', keyPress, false);
  window.addEventListener('keydown', keyDown, false);

  view.canvas.addEventListener('mousedown', canvasMouseDown, false);
  view.canvas.addEventListener('mousemove', canvasMouseMove, false);

  view.clear_canvas()
  //toggle_animation();
}
